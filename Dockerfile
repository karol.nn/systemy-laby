FROM --platform=linux/amd64 oven/bun:latest

WORKDIR /app
COPY . . 
RUN bun install
RUN bun run build

CMD bun run preview --host --port 80
